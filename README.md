cd ~/ros2_ws/src/project1
git add README.md
git commit -m "Add README.md with my personal information."
git push

# controlador estacion meteorologica

    - Autor: Lara de la Pena Jimenez
    - Fecha de ultima modificacion: 12/03/2023

## Introducción
En este proyecto, he eprendido a programar diferentes sensores del ámbito meteorológico que después de una serie de codigos se muestra en pantalla al abrir el programa de weather station y ejecutando el código aparecen una serie de datos 

### Ejecución del proyecto:

usando los codigos de la sesión 2 que hay que meter en las terminales, consiguimos llegar al resultado final del proyecto
es decir:
    1. abrimos nuestra terminal y escribimos lo siguiente:   
    cd ~/ros2_ws/
    colcon build --packages-select project1
    lo cual servirá para compilar nuestro proyecto y ver los fallos que tenemos (el paquete no se instalará hasta que el codigo compilado esté bien)
    
    2. ahora, toca abrir la aplicación mencionada en la introducción (weather station) con el siguiente código (el cual se puede pegar en la misma terminal que la del paso 1):
    ros2 run weather_station_ui weather_station_service

    3. Por último, ejecutamos nuestro programa en otra terminal:
    ros2 run project1 temperature_sensor
    ¡OJO!: depende de el sensor que queramos abrir hay que cambiar el nombre; es decir, si queremos ejecutar el sensor de visibilidad escribiremos lo siguiente
    ros2 run project1 visibility_sensor

****he intentado añadir la sesión 4, que para compilarlo los primeros dos pasos deberían hacerse iguales; sin embargo a la hora de ejecutar el programa deberemos poner lo siguiente:  
ros2 launch project1 project1_nodes_launch.yaml
y en este caso, deberían salir todos los sensores a la vez***

## Elementos excluidos

no he hecho la sesión 3 por falta de tiempo, aunque lo he intentado y no sabia hacer nada.
La sesión 4 he intentado hacerla, sin embargo me da error al ejecutarla

## Mejoras incluidas

con este proyecto he interiorizado mejor el lenguaje C++ y he descubierto el sistema operativo linux, con el cual nunca antes había trabajado

## Conclusiones
considero que no he tenido tiempo suficiente para conseguir entender bien el proyecto entero ni para entregar todas las sesiones; sin embargo me ha ayudado mucho para entender los sensores y el código de C++; a demás me ha otorgado cierta soltura a la hora de encontrar fallos en mi código.
