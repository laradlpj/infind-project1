#include <chrono>
#include <functional>
#include <memory>
#include <string>
#include <cmath>

#include "simulated_sensors_board_library/simulated_sensors_board.hpp"
#include "rclcpp/rclcpp.hpp"
#include "weather_station_msgs/msg/speed_meters_per_second.hpp"
#include "weather_station_msgs/srv/update_wind_speed.hpp"

class WindSpeedSensorPublisher : public rclcpp::Node
{
  public:
    WindSpeedSensorPublisher()
    : Node("wind_speed_sensor")
    {
      // Initialize wind_speed publisher
      publisher_ = this->create_publisher<weather_station_msgs::msg::SpeedMetersPerSecond>(
        "/weather_sensor/wind_speed", 10);
      // Initialize callback timer
      timer_ = this->create_wall_timer(
        std::chrono::milliseconds(500), 
        std::bind(&WindSpeedSensorPublisher::read_sensor_callback, this)
      );
      // Initialize simulation board
      board_ = SimulatedSensorsBoard();
      // Initialize service client
      client_ = this->create_client<weather_station_msgs::srv::UpdateWindSpeed>("/weather_station_service/update_wind_speed");
    }

  private:
    void read_sensor_callback()
    {
       // 1. Read value from library
        auto board= SimulatedSensorsBoard();

        auto wind_speed = board.read_pin_value (board.PIN_WIND_SPEED);
        std::cout << "WindSpeed: " << wind_speed << std::endl;

      // 2. Convert value to metric scale and round to 2 decimals
          auto scaled_wind_speed = static_cast< float > (90 * (static_cast< float >(wind_speed - 0)/ static_cast< float >(1024 - 0)) + (-20));
          std::cout << std::setprecision(2) << scaled_wind_speed;

        // 3. Publish the value to the topic
        auto message = weather_station_msgs::msg::SpeedMetersPerSecond();
        message.meters_per_second = floor (scaled_wind_speed *100)/100;
        RCLCPP_INFO(this->get_logger(), "Publishing wind_speed: '%.2lf' ",
          message.meters_per_second);
          publisher_->publish(message);

        // 4. Call the weather_station service to display the value in the UI
    auto request = std::make_shared<weather_station_msgs::srv::UpdateWindSpeed::Request>();
    request->update = message;
    while (!client_->wait_for_service(std::chrono::seconds(1))) {
    if (!rclcpp::ok()) {
        RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Interrupted while waiting for the service. Exiting.");
        return;
    }
    RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "service not available, waiting again...");
}        
    }

    rclcpp::TimerBase::SharedPtr timer_;
    rclcpp::Publisher<weather_station_msgs::msg::SpeedMetersPerSecond>::SharedPtr publisher_;
    SimulatedSensorsBoard board_;
    rclcpp::Client<weather_station_msgs::srv::UpdateWindSpeed>::SharedPtr client_;
};

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<WindSpeedSensorPublisher>());
  rclcpp::shutdown();
  return 0;
}