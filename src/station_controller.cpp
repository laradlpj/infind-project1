#include <cstdio>
#include <iostream>
#include "simulated_sensors_board_library/simulated_sensors_board.hpp"

int main(int argc, char ** argv)
{
  (void) argc;
  (void) argv;

  auto board = SimulatedSensorsBoard();

  auto temperature = board.read_pin_value(board.PIN_TEMPERATURE);
  std::cout << "Temperature: " << temperature << std::endl;

  auto pressure = board.read_pin_value(board.PIN_PRESSURE);
  std::cout << "Pressure: " << pressure << std::endl;
  
  return 0;
}

