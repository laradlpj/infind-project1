#include <chrono>
#include <functional>
#include <memory>
#include <string>
#include <cmath>

#include "simulated_sensors_board_library/simulated_sensors_board.hpp"
#include "rclcpp/rclcpp.hpp"
#include "weather_station_msgs/msg/meters.hpp"
#include "weather_station_msgs/srv/update_visibility.hpp"

class VisibilitySensorPublisher : public rclcpp::Node
{
  public:
    VisibilitySensorPublisher()
    : Node("visibility_sensor")
    {
      // Initialize visibility publisher
      publisher_ = this->create_publisher<weather_station_msgs::msg::Meters>(
        "/weather_sensor/visibility", 10);
      // Initialize callback timer
      timer_ = this->create_wall_timer(
        std::chrono::milliseconds(500), 
        std::bind(&VisibilitySensorPublisher::read_sensor_callback, this)
      );
      // Initialize simulation board
      board_ = SimulatedSensorsBoard();
      // Initialize service client
      client_ = this->create_client<weather_station_msgs::srv::UpdateVisibility>("/weather_station_service/update_visibility");
    }

  private:
    void read_sensor_callback()
    {
       // 1. Read value from library
        auto board= SimulatedSensorsBoard();

        auto visibility = board.read_pin_value (board.PIN_VISIBILITY);
        std::cout << "visibility: " << visibility << std::endl;

      // 2. Convert value to metric scale and round to 2 decimals
          auto scaled_visibility = static_cast< float > (100 * (static_cast< float >(visibility - 0)/ static_cast< float >(1024 - 0)) + (-20));
          std::cout << std::setprecision(2) << scaled_visibility;

        // 3. Publish the value to the topic
        auto message = weather_station_msgs::msg::Meters();
        message.meters = floor (scaled_visibility *100)/100;
        RCLCPP_INFO(this->get_logger(), "Publishing visibility: '%.2lf' ",
          message.meters);
          publisher_->publish(message);

        // 4. Call the weather_station service to display the value in the UI
    auto request = std::make_shared<weather_station_msgs::srv::UpdateVisibility::Request>();
    request->update = message;
    while (!client_->wait_for_service(std::chrono::seconds(1))) {
    if (!rclcpp::ok()) {
        RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Interrupted while waiting for the service. Exiting.");
        return;
    }
    RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "service not available, waiting again...");
}        
    }

    rclcpp::TimerBase::SharedPtr timer_;
    rclcpp::Publisher<weather_station_msgs::msg::Meters>::SharedPtr publisher_;
    SimulatedSensorsBoard board_;
    rclcpp::Client<weather_station_msgs::srv::UpdateVisibility>::SharedPtr client_;
};

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<VisibilitySensorPublisher>());
  rclcpp::shutdown();
  return 0;
}
